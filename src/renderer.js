import Keyboard from 'simple-keyboard'
import 'simple-keyboard/build/css/index.css'
import './index.css'

const webview = document.querySelector('webview')
webview.openDevTools()

// doesn't work
webview.addEventListener('found-in-page', (e) => {
    console.log(e)
    webview.stopFindInPage('keepSelection')
})

const req = webview.findInPage('input')
console.log(req)
/// /

webview.executeJavaScript(
    document
        .querySelector('input')
        .addEventListener('onClick', () => console.log('found input'))
)

const defaultTheme = 'hg-theme-default'

const keyboard = new Keyboard({
    theme: defaultTheme,
    onChange: (input) => onChange(input),
    onKeyPress: (button) => onKeyPress(button),
})

const inputDOM = document.querySelector('.input')

function onChange(input) {
    document.querySelector('.input').value = input
    console.log('Input changed', input)
}

function onKeyPress(button) {
    /**
     *    * If you want to handle the shift and caps lock buttons
     *       */
    if (button === '{shift}' || button === '{lock}') handleShift()
}

function handleShift() {
    const currentLayout = keyboard.options.layoutName
    const shiftToggle = currentLayout === 'default' ? 'shift' : 'default'

    keyboard.setOptions({
        layoutName: shiftToggle,
    })
}

function showKeyboard() {
    document.querySelector('.simple-keyboard').style.visibility = 'visible'
    keyboard.setOptions({
        theme: `${defaultTheme} show-keyboard`,
    })
}

function hideKeyboard() {
    document.querySelector('.simple-keyboard').style.visibility = 'hidden'
}

hideKeyboard()

/**
 *  * Keyboard show
 *   */
inputDOM.addEventListener('focus', () => {
    showKeyboard()
})

/**
 *  * Keyboard show toggle
 *   */
document.addEventListener('click', (event) => {
    if (
        /**
         *      * Hide the keyboard when you're not clicking it or when clicking an input
         *           * If you have installed a "click outside" library, please use that instead.
         *                */
        keyboard.options.theme.includes('show-keyboard') &&
        !event.target.className.includes('input') &&
        !event.target.className.includes('hg-button') &&
        !event.target.className.includes('hg-row') &&
        !event.target.className.includes('simple-keyboard')
    ) {
        hideKeyboard()
    }
})

/**
 *  * Update simple-keyboard when input is changed directly
 *   */
document.querySelector('.input').addEventListener('input', (event) => {
    keyboard.setInput(event.target.value)
})
